var express = require('express');
var router = express.Router();

/* GET add form. */
router.get('/', function(req, res, next) {
    render(res);
});

/* GET users listing. */
router.post('/', function(req, res, next) {
    let result = +req.body.a * +req.body.b;
    render(res, req.body.a, req.body.b, result);
});

function render(res, a, b, result) {
    res.render('multiply', {title: 'Calculator: Multiply', a: a, b: b, result: result});
}

module.exports = router;